#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
script to simulate two dices of six
sides and calculate the distribution
'''


import pandas as pd
import random
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

columns = ['dice1','dice2','pair']
sim_results = pd.DataFrame(columns=columns)


for dice in range(10000):
    dice1 = random.randint(1,6)
    dice2 = random.randint(1,6)
    #print str(dice1) + ' ' + str(dice2)
    sim_results = sim_results.append({"dice1":dice1,'dice2':dice2}, ignore_index=True)


sim_results = sim_results.astype('float64')
sim_results['dices_sum'] = sim_results.iloc[:,:2].sum(axis=1)
sim_results.head

# make an histogram of the total obtained, dice1 plus dice2
sim_results.hist(column='dices_sum', bins=11, range=[2,13], align='mid')
plt.xticks(np.arange(2, 13, 1.0))
plt.show()
plt.savefig('suma_de_los_dados.pdf')

# Combine the tree columns
combined = sim_results['dice1']
combined = combined.append(sim_results['dice2'], ignore_index=True)
combined = combined.append(sim_results['dices_sum'], ignore_index=True)
combined.head


# plot a histogram with the results of dice1, dice2 and the total
combined.hist(bins=12, range=[1,13], align='mid')
plt.xticks(np.arange(1, 13, 1.0))
plt.show()
plt.savefig('suma_de_los_dados_e_individuales.pdf')

# calculate the numbers of pairs obtanied
for index, row in sim_results.iterrows():
    if row['dice1'] == row['dice2']:
        row['pair'] = 1.0
    else:
        row['pair'] = 0.0

sim_results['pair'].sum()

sim_results.hist(column='pair', align='mid')
plt.xticks(np.arange(0, 2, 1.0))
plt.show()
plt.savefig('pares.pdf')

# Finally, lets look at the distribution of doubles

# subset a from results the results that are doubles
sim_results_doubles = sim_results.loc[sim_results['pair']==1.0]
sim_results_doubles.hist(column='dice1', bins=6, range=[1,7], align='mid')
plt.xticks(np.arange(1, 7, 1.0))
plt.show()
plt.savefig('pares_por_numero.pdf')